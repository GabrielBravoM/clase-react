import { useState } from "react";
import Text from "./components/text";
import Button from "./components/button";

function App() {
  const [contador, setContador] = useState(0);

  const aumentar = () => {
    const nuevoContador = contador + 1;
    setContador(nuevoContador);
  };

  const disminuir = () => {
    const nuevoContador = contador - 1;
    setContador(nuevoContador);
  };

  return (
    <div className="App">
      <Text texto="Hola Tito" size={50} />
      <Text texto="Hola Tacto" size={100} />

      <Button value="Sumar 1" onClick={aumentar} />
      <Button value="Restar 1" onClick={disminuir} />
      <br />
      <Text texto={contador} size={200} />
    </div>
  );
}

export default App;

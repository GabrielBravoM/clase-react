const Text = (props) => {
  return <h1 style={{ fontSize: props.size }}>{props.texto}</h1>;
};

export default Text;
